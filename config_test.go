package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/levenlabs/go-llog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	configPriority1 = "/.config/gitlab-tui/.gitlab-tui.json"
	configPriority2 = "/.config/.gitlab-tui.json"
	configPriority3 = "/.gitlab-tui.json"
	ogTmpDir        = os.Getenv("TMPDIR")
)

func setUp() {
	dir, err := os.Getwd()
	if err != nil {
		// this should never trip, but if it does then we are kinda screwed
		llog.Fatal("unable to get current working directory")
	}
	os.Setenv("TMPDIR", dir+"/tmp/")
	os.Setenv("GITLABTUI_CONFIG_DIR", os.TempDir())
	os.Setenv("HOME", dir+"/tmp/")

	configPriority1 = "/.config/gitlab-tui/.gitlab-tui.json"
	err = os.MkdirAll(filepath.Join(os.TempDir(), ".config/gitlab-tui/"), os.ModePerm)
	if err != nil {
		llog.Fatal("unable to create tmp dirs for tests")
	}
}

func tearDown() {
	err := os.RemoveAll(os.TempDir())
	if err != nil {
		llog.Fatal("unable to remove everything in the temp dir")
	}
	os.Setenv("TMPDIR", ogTmpDir)
}

// TODO: TestSeekConfiguration
func TestSeekConfiguration(t *testing.T) {
	setUp()
	// TEST: If config file exists in basic locations
	// TESTTODO: create a config files in config locations ($HOME/.config/gitlab-tui, $HOME/.config, $HOME/)
	// TEST: priority of config files left to right:
	//       $HOME/.config/gitlab-tui/.gitlab-tui.json
	//       remove config files as we go
	expectedFile1 := filepath.Join(os.TempDir(), configPriority1)
	_, err := os.Create(expectedFile1)
	require.NoError(t, err, llog.ErrKV(err))

	// if CONFIG_DIR env is not set, look here
	os.Setenv("GITLABTUI_CONFIG_DIR", "")
	fileExists, file := seekConfiguration()
	assert.Equal(t, expectedFile1, file)
	assert.Equal(t, true, fileExists)
	err = os.Remove(filepath.Join(os.TempDir() + configPriority1))
	require.NoError(t, err, llog.ErrKV(err))

	// if the GITLABTUI_CONFIG_DIR env variable is set then check there
	// file does not exist
	// file exists
	randomDir := filepath.Join(os.TempDir(), "totallyrandomdir")
	err = os.Mkdir(randomDir, os.ModePerm)
	require.NoError(t, err, llog.ErrWithKV(err))
	os.Setenv("GITLABTUI_CONFIG_DIR", filepath.Join(os.TempDir(), "totallyrandomdir"))
	fileExists, file = seekConfiguration()
	assert.Equal(t, filepath.Join(randomDir, ".gitlab-tui.json"), file)
	assert.Equal(t, false, fileExists)
	require.NoError(t, err, llog.ErrWithKV(err))

	// create the test file in config_dir
	_, err = os.Create(filepath.Join(randomDir, ".gitlab-tui.json"))
	require.NoError(t, err, llog.ErrWithKV(err))
	fileExists, file = seekConfiguration()
	assert.Equal(t, filepath.Join(randomDir, ".gitlab-tui.json"), file)
	assert.Equal(t, true, fileExists)
	require.NoError(t, err, llog.ErrWithKV(err))
	// clean up our act
	tearDown()
}

// TODO: TestCreateConfiguration
func TestCreateConfiguration(t *testing.T) {
	err := createConfiguation()
	require.NoError(t, err, llog.ErrKV(err))
}

// TODO: TestParseConfiguration
func TestParseConfiguration(t *testing.T) {
	res, err := parseConfiguration()
	require.NoError(t, err, llog.ErrKV(err))
	assert.NotNil(t, res)
}

// TODO: ask user for inputs
func TestGatherInfo(t *testing.T) {
	err := gatherUserInfo()
	require.NoError(t, err, llog.ErrKV(err))
}
