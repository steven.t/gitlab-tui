package main

import "errors"

var (
	errNotImplemented = errors.New("Not yet implemented")
	errFileSystemFunk = errors.New("Something went wrong reading from the filesystem")
)
