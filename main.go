package main

import (
	"fmt"
	"os"

	"github.com/levenlabs/go-llog"
)

func main() {
	dir, err := os.UserHomeDir()
	if err != nil {
		llog.ErrWithKV(err)
	}
	fmt.Printf("HomeDir: %s", dir)
	tempdir := os.Getenv("TEMPDIR")
	fmt.Println(tempdir)

}
