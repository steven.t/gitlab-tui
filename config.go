package main

import (
	"os"
	"path/filepath"

	"github.com/levenlabs/go-llog"
)

// glabConfiguration is used store the configuration that willl get
// saved to a json file
type glabConfiguration struct {
	token    string  `json:"token"`
	groups   []asset `json:"groups"`
	projects []asset `json:"projects"`
}

type asset struct {
	name string `json:"name"`
}

// Configure returns a pointer to a glabConfiguration instance.
// On startup this gets called, and looks for a configuration file
// if one does not exist, then it will ask for user inputs to create one
func Configure() *glabConfiguration {
	return &glabConfiguration{}
}

// TODO: func seekConfiguration
func seekConfiguration() (bool, string) {
	// get the user's home directory
	homeDir, err := os.UserHomeDir()
	if err != nil {
		llog.ErrWithKV(err)
	}
	// check if environment variable exists
	// if not, set to user's home directory
	// if the this env variable is set then we want to look for the config in
	// directory specified
	configDir := os.Getenv("GITLABTUI_CONFIG_DIR")
	if configDir == "" {
		if _, err := os.Stat(filepath.Join(homeDir + "/.config/gitlab-tui/.gitlab-tui.json")); err == nil {
			return true, filepath.Join(homeDir + "/.config/gitlab-tui/.gitlab-tui.json")
		}
		return false, filepath.Join(homeDir + "/.config/gitlab-tui/.gitlab-tui.json")
	}
	_, err = os.Stat(filepath.Join(configDir, "/.gitlab-tui.json"))
	return err == nil, filepath.Join(configDir + "/.gitlab-tui.json")
}

// TODO: func parseConfiguation
func parseConfiguration() (*glabConfiguration, error) {
	return &glabConfiguration{}, errNotImplemented
}

// TODO: create empty configuration
func createConfiguation() error {
	return errNotImplemented
}

// TODO: ask user for inputs
func gatherUserInfo() error {
	return errNotImplemented
}
