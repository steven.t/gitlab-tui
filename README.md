# Gitlab-TUI

## Goals:

 - [ ] Build with Go, Lipgloss/Bubbletea
 - [ ] Manage issues, projects, view group boards for milestones
 - [ ] Create Issues, projects
 - [ ] View Issues assigned to us
 - [ ] View Issues where we're a reviewer
 - [ ] See Merge Requests Assigned to me
 - [ ] See open MR's that we're a reviewer for 

